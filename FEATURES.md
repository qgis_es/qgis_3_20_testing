# qgis_3_20_testing

Recursos e información de las pruebas realizadas por miembros de la [Asociación QGIS España](https://www.qgis.es/) para QGIS 3.20.0

![QGIS 3.20](https://qgis.org/en/_images/712c5f48a25ce79a413e9cc34336e05100b7f0c1.png)

## Objetivo

Actividad organizada la Asociación QGIS España probar y documentar las características de la version QGIS 3.20.0 Zürich (2021-06-18)

El listado de novedades se puede consultar en el siguiente enlace https://qgis.org/en/site/forusers/visualchangelog320/index.html

## Participantes

- Patricio Soriano Castro [Linkedin](https://www.linkedin.com/in/patriciosorianocastro/) -  [Twitter](https://twitter.com/sigdeletras) - [Web](http://sigdeletras.com/)
- 
- 

## Características


### 01 Feature: Additional options for opening attribute tables

Categoría: **General**

[Enlace a Changelog](https://qgis.org/en/site/forusers/visualchangelog320/index.html#feature-additional-options-for-opening-attribute-tables)

**Comentarios**

**Recursos**

Carpeta de recursos [/recursos/](/recursos/)

Enlaces a vídeos

![GUI for dynamic SVGs](/recursos/01/ejemplo.mp4)

### 02 Feature: Set size for all columns in attribute table

Categoría: **General**

[Enlace a Changelog](https://qgis.org/en/site/forusers/visualchangelog320/index.html#feature-additional-options-for-opening-attribute-tables)

**Comentarios**

**Recursos**

Carpeta de recursos [/recursos/](/recursos/)

Enlaces a vídeos

![GUI for dynamic SVGs](/recursos/01/ejemplo.mp4)

### 03 Feature: Export/import of authentication configurations made easy

Categoría: **General**

[Enlace a Changelog](https://qgis.org/en/site/forusers/visualchangelog320/index.html#feature-additional-options-for-opening-attribute-tables)

**Comentarios**

**Recursos**

Carpeta de recursos [/recursos/](/recursos/)

Enlaces a vídeos

![GUI for dynamic SVGs](/recursos/01/ejemplo.mp4)

### 04 Feature: Temporal navigation steps 

Categoría: **Temporal**

[Enlace a Changelog](https://qgis.org/en/site/forusers/visualchangelog320/index.html#feature-additional-options-for-opening-attribute-tables)

**Comentarios**

**Recursos**

Carpeta de recursos [/recursos/](/recursos/)

Enlaces a vídeos

![GUI for dynamic SVGs](/recursos/01/ejemplo.mp4)

### 05 Feature: Improved WMS-T settings

Categoría: **Temporal**

[Enlace a Changelog](https://qgis.org/en/site/forusers/visualchangelog320/index.html#feature-additional-options-for-opening-attribute-tables)

**Comentarios**

**Recursos**

Carpeta de recursos [/recursos/](/recursos/)

Enlaces a vídeos

![GUI for dynamic SVGs](/recursos/01/ejemplo.mp4)

### 06 Feature: Horizontal mouse wheel temporal navigation

Categoría: **Temporal**

[Enlace a Changelog](https://qgis.org/en/site/forusers/visualchangelog320/index.html#feature-additional-options-for-opening-attribute-tables)

**Comentarios**

**Recursos**

Carpeta de recursos [/recursos/](/recursos/)

Enlaces a vídeos

![GUI for dynamic SVGs](/recursos/01/ejemplo.mp4)

### 07 Feature: Nominatim Geocoder Integration 

Categoría: **Map Tools**

[Enlace a Changelog](https://qgis.org/en/site/forusers/visualchangelog320/index.html#feature-additional-options-for-opening-attribute-tables)

**Comentarios**

**Recursos**

Carpeta de recursos [/recursos/](/recursos/)

Enlaces a vídeos

![GUI for dynamic SVGs](/recursos/01/ejemplo.mp4)

### 08 Feature: Nominatim Geocoder Integration 

Categoría: **Map Tools**

[Enlace a Changelog](https://qgis.org/en/site/forusers/visualchangelog320/index.html#feature-additional-options-for-opening-attribute-tables)

**Comentarios**

**Recursos**

Carpeta de recursos [/recursos/](/recursos/)

Enlaces a vídeos

![GUI for dynamic SVGs](/recursos/01/ejemplo.mp4)

### 09 Processing history dialog improvements

Categoría: **User Interface**

[Enlace a Changelog](https://qgis.org/en/site/forusers/visualchangelog320/index.html#feature-additional-options-for-opening-attribute-tables)

**Comentarios**

**Recursos**

Carpeta de recursos [/recursos/](/recursos/)

Enlaces a vídeos

![GUI for dynamic SVGs](/recursos/01/ejemplo.mp4)


### 10 Feature: Map layer icons in the expression builder dialog

Categoría: **User Interface**
[Enlace a Changelog](https://qgis.org/en/site/forusers/visualchangelog320/index.html#feature-additional-options-for-opening-attribute-tables)

**Comentarios**

**Recursos**

Carpeta de recursos [/recursos/](/recursos/)

Enlaces a vídeos

![GUI for dynamic SVGs](/recursos/01/ejemplo.mp4)


### 11 Feature: SVG browser filtering

Categoría: **User Interface**

[Enlace a Changelog](https://qgis.org/en/site/forusers/visualchangelog320/index.html#feature-additional-options-for-opening-attribute-tables)

**Comentarios**

**Recursos**

Carpeta de recursos [/recursos/](/recursos/)

Enlaces a vídeos

![GUI for dynamic SVGs](/recursos/01/ejemplo.mp4)


### 12 Feature: Interpolated line symbol layer type for vector layers

Categoría: **Symbology**

[Enlace a Changelog](https://qgis.org/en/site/forusers/visualchangelog320/index.html#feature-additional-options-for-opening-attribute-tables)

**Comentarios**

**Recursos**

Carpeta de recursos [/recursos/](/recursos/)

Enlaces a vídeos

![GUI for dynamic SVGs](/recursos/01/ejemplo.mp4)


### 13 Feature: Trim the simple line symbol

Categoría: **Symbology**

[Enlace a Changelog](https://qgis.org/en/site/forusers/visualchangelog320/index.html#feature-additional-options-for-opening-attribute-tables)

**Comentarios**

**Recursos**

Carpeta de recursos [/recursos/](/recursos/)

Enlaces a vídeos

![GUI for dynamic SVGs](/recursos/01/ejemplo.mp4)


###  14 Feature: New “Embedded styling” renderer with OGR feature styles support

Categoría: **Symbology**

[Enlace a Changelog](https://qgis.org/en/site/forusers/visualchangelog320/index.html#feature-additional-options-for-opening-attribute-tables)

**Comentarios**

**Recursos**

Carpeta de recursos [/recursos/](/recursos/)

Enlaces a vídeos

![GUI for dynamic SVGs](/recursos/01/ejemplo.mp4)


###  15 Feature: New shapes and cap styles for ellipse markers

Categoría: **Symbology**

[Enlace a Changelog](https://qgis.org/en/site/forusers/visualchangelog320/index.html#feature-additional-options-for-opening-attribute-tables)

**Comentarios**

**Recursos**

Carpeta de recursos [/recursos/](/recursos/)

Enlaces a vídeos

![GUI for dynamic SVGs](/recursos/01/ejemplo.mp4)


### 16 Feature: Set cap styles for simple markers

Categoría: **Symbology**

[Enlace a Changelog](https://qgis.org/en/site/forusers/visualchangelog320/index.html#feature-additional-options-for-opening-attribute-tables)

**Comentarios**

**Recursos**

Carpeta de recursos [/recursos/](/recursos/)

Enlaces a vídeos

![GUI for dynamic SVGs](/recursos/01/ejemplo.mp4)


### 17 Feature: Allow pen cap style to be set for ellipse marker symbol layers

Categoría: **Symbology**

[Enlace a Changelog](https://qgis.org/en/site/forusers/visualchangelog320/index.html#feature-additional-options-for-opening-attribute-tables)

**Comentarios**

**Recursos**

Carpeta de recursos [/recursos/](/recursos/)

Enlaces a vídeos

![GUI for dynamic SVGs](/recursos/01/ejemplo.mp4)


### 18 Feature: Polygon rings @geometry_ring_num variable

Categoría: **Symbology**

[Enlace a Changelog](https://qgis.org/en/site/forusers/visualchangelog320/index.html#feature-additional-options-for-opening-attribute-tables)

**Comentarios**

**Recursos**

Carpeta de recursos [/recursos/](/recursos/)

Enlaces a vídeos

![GUI for dynamic SVGs](/recursos/01/ejemplo.mp4)



### 19 Feature: Fill symbols for label background shields

Categoría: **Labelling**

[Enlace a Changelog](https://qgis.org/en/site/forusers/visualchangelog320/index.html#feature-additional-options-for-opening-attribute-tables)

**Comentarios**

**Recursos**

Carpeta de recursos [/recursos/](/recursos/)

Enlaces a vídeos

![GUI for dynamic SVGs](/recursos/01/ejemplo.mp4)


### 20 Feature: Blending mode settings for label callouts

Categoría: **Labelling**

[Enlace a Changelog](https://qgis.org/en/site/forusers/visualchangelog320/index.html#feature-additional-options-for-opening-attribute-tables)

**Comentarios**

**Recursos**

Carpeta de recursos [/recursos/](/recursos/)

Enlaces a vídeos

![GUI for dynamic SVGs](/recursos/01/ejemplo.mp4)


### 21 Feature: Anchor line labels by entire line

Categoría: **Labelling**

[Enlace a Changelog](https://qgis.org/en/site/forusers/visualchangelog320/index.html#feature-additional-options-for-opening-attribute-tables)

**Comentarios**

**Recursos**

Carpeta de recursos [/recursos/](/recursos/)

Enlaces a vídeos

![GUI for dynamic SVGs](/recursos/01/ejemplo.mp4)


### 22 Feature: Balloon callout corner radius

Categoría: **Labelling**

[Enlace a Changelog](https://qgis.org/en/site/forusers/visualchangelog320/index.html#feature-additional-options-for-opening-attribute-tables)

**Comentarios**

**Recursos**

Carpeta de recursos [/recursos/](/recursos/)

Enlaces a vídeos

![GUI for dynamic SVGs](/recursos/01/ejemplo.mp4)


### 23 Feature: Balloon (speech bubble) callouts

Categoría: **Labelling**

[Enlace a Changelog](https://qgis.org/en/site/forusers/visualchangelog320/index.html#feature-additional-options-for-opening-attribute-tables)

**Comentarios**

**Recursos**

Carpeta de recursos [/recursos/](/recursos/)

Enlaces a vídeos

![GUI for dynamic SVGs](/recursos/01/ejemplo.mp4)


### 24 Feature: Curved line callout style

Categoría: **Labelling**

[Enlace a Changelog](https://qgis.org/en/site/forusers/visualchangelog320/index.html#feature-additional-options-for-opening-attribute-tables)

**Comentarios**

**Recursos**

Carpeta de recursos [/recursos/](/recursos/)

Enlaces a vídeos

![GUI for dynamic SVGs](/recursos/01/ejemplo.mp4)


### 25 Feature: Highlight pinned callout start or end points

Categoría: **Labelling**

[Enlace a Changelog](https://qgis.org/en/site/forusers/visualchangelog320/index.html#feature-additional-options-for-opening-attribute-tables)

**Comentarios**

**Recursos**

Carpeta de recursos [/recursos/](/recursos/)

Enlaces a vídeos

![GUI for dynamic SVGs](/recursos/01/ejemplo.mp4)


### 26 Feature: Auto-creation of callout auxiliary fields

Categoría: **Labelling**

[Enlace a Changelog](https://qgis.org/en/site/forusers/visualchangelog320/index.html#feature-additional-options-for-opening-attribute-tables)

**Comentarios**

**Recursos**

Carpeta de recursos [/recursos/](/recursos/)

Enlaces a vídeos

![GUI for dynamic SVGs](/recursos/01/ejemplo.mp4)


### 27 Feature: Callout snapping

Categoría: **Labelling**

[Enlace a Changelog](https://qgis.org/en/site/forusers/visualchangelog320/index.html#feature-additional-options-for-opening-attribute-tables)

**Comentarios**

**Recursos**

Carpeta de recursos [/recursos/](/recursos/)

Enlaces a vídeos

![GUI for dynamic SVGs](/recursos/01/ejemplo.mp4)


### 28 Feature: Toggle label layers shortcut

Categoría: **Labelling**

[Enlace a Changelog](https://qgis.org/en/site/forusers/visualchangelog320/index.html#feature-additional-options-for-opening-attribute-tables)

**Comentarios**

**Recursos**

Carpeta de recursos [/recursos/](/recursos/)

Enlaces a vídeos

![GUI for dynamic SVGs](/recursos/01/ejemplo.mp4)


### 29 Feature: Data defined callout start and end points

Categoría: **Labelling**

[Enlace a Changelog](https://qgis.org/en/site/forusers/visualchangelog320/index.html#feature-additional-options-for-opening-attribute-tables)

**Comentarios**

**Recursos**

Carpeta de recursos [/recursos/](/recursos/)

Enlaces a vídeos

![GUI for dynamic SVGs](/recursos/01/ejemplo.mp4)


### 30 Feature: Allow callouts to be interactively moved using the Move Label tool

Categoría: **Labelling**

[Enlace a Changelog](https://qgis.org/en/site/forusers/visualchangelog320/index.html#feature-additional-options-for-opening-attribute-tables)

**Comentarios**

**Recursos**

Carpeta de recursos [/recursos/](/recursos/)

Enlaces a vídeos

![GUI for dynamic SVGs](/recursos/01/ejemplo.mp4)


### 31 Feature: Support for remote datasets (EPT)

Categoría: **Point Clouds**

[Enlace a Changelog](https://qgis.org/en/site/forusers/visualchangelog320/index.html#feature-additional-options-for-opening-attribute-tables)

**Comentarios**

**Recursos**

Carpeta de recursos [/recursos/](/recursos/)

Enlaces a vídeos

![GUI for dynamic SVGs](/recursos/01/ejemplo.mp4)


### 32 Feature: “Convert to Static Text” option

Categoría: **Print Layouts**

[Enlace a Changelog](https://qgis.org/en/site/forusers/visualchangelog320/index.html#feature-additional-options-for-opening-attribute-tables)

**Comentarios**

**Recursos**

Carpeta de recursos [/recursos/](/recursos/)

Enlaces a vídeos

![GUI for dynamic SVGs](/recursos/01/ejemplo.mp4)


### 33 Feature: Improvement to XYZ layers previewed in map items

Categoría: **Print Layouts**

[Enlace a Changelog](https://qgis.org/en/site/forusers/visualchangelog320/index.html#feature-additional-options-for-opening-attribute-tables)

**Comentarios**

**Recursos**

Carpeta de recursos [/recursos/](/recursos/)

Enlaces a vídeos

![GUI for dynamic SVGs](/recursos/01/ejemplo.mp4)


### 34 Feature: length3D Function

Categoría: **Expressions**

[Enlace a Changelog](https://qgis.org/en/site/forusers/visualchangelog320/index.html#feature-additional-options-for-opening-attribute-tables)

**Comentarios**

**Recursos**

Carpeta de recursos [/recursos/](/recursos/)

Enlaces a vídeos

![GUI for dynamic SVGs](/recursos/01/ejemplo.mp4)


### 35 Feature: Extended array expression functions

Categoría: **Expressions**

[Enlace a Changelog](https://qgis.org/en/site/forusers/visualchangelog320/index.html#feature-additional-options-for-opening-attribute-tables)

**Comentarios**

**Recursos**

Carpeta de recursos [/recursos/](/recursos/)

Enlaces a vídeos

![GUI for dynamic SVGs](/recursos/01/ejemplo.mp4)


### 36 Feature: MIME Type expression function

Categoría: **Expressions**

[Enlace a Changelog](https://qgis.org/en/site/forusers/visualchangelog320/index.html#feature-additional-options-for-opening-attribute-tables)

**Comentarios**

**Recursos**

Carpeta de recursos [/recursos/](/recursos/)

Enlaces a vídeos

![GUI for dynamic SVGs](/recursos/01/ejemplo.mp4)


### 37 Feature: Select attributes from the largest geometry when merging

Categoría: **Digitising**

[Enlace a Changelog](https://qgis.org/en/site/forusers/visualchangelog320/index.html#feature-additional-options-for-opening-attribute-tables)

**Comentarios**

**Recursos**

Carpeta de recursos [/recursos/](/recursos/)

Enlaces a vídeos

![GUI for dynamic SVGs](/recursos/01/ejemplo.mp4)


### 38 Feature: Streaming digitizing mode

Categoría: **Digitising**

[Enlace a Changelog](https://qgis.org/en/site/forusers/visualchangelog320/index.html#feature-additional-options-for-opening-attribute-tables)

**Comentarios**

**Recursos**

Carpeta de recursos [/recursos/](/recursos/)

Enlaces a vídeos

![GUI for dynamic SVGs](/recursos/01/ejemplo.mp4)


### 39 Feature: New “Line Endpoints” snapping option

Categoría: **Digitising**

[Enlace a Changelog](https://qgis.org/en/site/forusers/visualchangelog320/index.html#feature-additional-options-for-opening-attribute-tables)

**Comentarios**

**Recursos**

Carpeta de recursos [/recursos/](/recursos/)

Enlaces a vídeos

![GUI for dynamic SVGs](/recursos/01/ejemplo.mp4)


### 40 Feature: Select vertices by polygon

Categoría: **Digitising**

[Enlace a Changelog](https://qgis.org/en/site/forusers/visualchangelog320/index.html#feature-additional-options-for-opening-attribute-tables)

**Comentarios**

**Recursos**

Carpeta de recursos [/recursos/](/recursos/)

Enlaces a vídeos

![GUI for dynamic SVGs](/recursos/01/ejemplo.mp4)


### 41 Feature: Layer tree warning icon for layers with CRS inaccuracies

Categoría: **Data Management**

[Enlace a Changelog](https://qgis.org/en/site/forusers/visualchangelog320/index.html#feature-additional-options-for-opening-attribute-tables)

**Comentarios**

**Recursos**

Carpeta de recursos [/recursos/](/recursos/)

Enlaces a vídeos

![GUI for dynamic SVGs](/recursos/01/ejemplo.mp4)


### 42 Feature: Basic support for dynamic CRS coordinate epoch

Categoría: **Data Management**

[Enlace a Changelog](https://qgis.org/en/site/forusers/visualchangelog320/index.html#feature-additional-options-for-opening-attribute-tables)

**Comentarios**

**Recursos**

Carpeta de recursos [/recursos/](/recursos/)

Enlaces a vídeos

![GUI for dynamic SVGs](/recursos/01/ejemplo.mp4)


### 43 Feature: Projection information improvements

Categoría: **Data Management**

[Enlace a Changelog](https://qgis.org/en/site/forusers/visualchangelog320/index.html#feature-additional-options-for-opening-attribute-tables)

**Comentarios**

**Recursos**

Carpeta de recursos [/recursos/](/recursos/)

Enlaces a vídeos

![GUI for dynamic SVGs](/recursos/01/ejemplo.mp4)


### 44 Feature: Datum ensemble CRS warnings

Categoría: **Data Management**

[Enlace a Changelog](https://qgis.org/en/site/forusers/visualchangelog320/index.html#feature-additional-options-for-opening-attribute-tables)

**Comentarios**

**Recursos**

Carpeta de recursos [/recursos/](/recursos/)

Enlaces a vídeos

![GUI for dynamic SVGs](/recursos/01/ejemplo.mp4)


### 45 Feature: Persist layer metadata in vector file exports

Categoría: **Data Management**

[Enlace a Changelog](https://qgis.org/en/site/forusers/visualchangelog320/index.html#feature-additional-options-for-opening-attribute-tables)

**Comentarios**

**Recursos**

Carpeta de recursos [/recursos/](/recursos/)

Enlaces a vídeos

![GUI for dynamic SVGs](/recursos/01/ejemplo.mp4)


### 46 Feature: Layer notes

Categoría: **Data Management**

[Enlace a Changelog](https://qgis.org/en/site/forusers/visualchangelog320/index.html#feature-additional-options-for-opening-attribute-tables)

**Comentarios**

**Recursos**

Carpeta de recursos [/recursos/](/recursos/)

Enlaces a vídeos

![GUI for dynamic SVGs](/recursos/01/ejemplo.mp4)


### 47 Feature: Automatically load .shp.xml metadata

Categoría: **Data Management**

[Enlace a Changelog](https://qgis.org/en/site/forusers/visualchangelog320/index.html#feature-additional-options-for-opening-attribute-tables)

**Comentarios**

**Recursos**

Carpeta de recursos [/recursos/](/recursos/)

Enlaces a vídeos

![GUI for dynamic SVGs](/recursos/01/ejemplo.mp4)


### 48 Feature: Automatically translate layer data from ESRI Filegeodatabases to QGIS layer metadata

Categoría: **Data Management**

[Enlace a Changelog](https://qgis.org/en/site/forusers/visualchangelog320/index.html#feature-additional-options-for-opening-attribute-tables)

**Comentarios**

**Recursos**

Carpeta de recursos [/recursos/](/recursos/)

Enlaces a vídeos

![GUI for dynamic SVGs](/recursos/01/ejemplo.mp4)


### 49 Feature: Read field domains from datasets

Categoría: **Data Management**

[Enlace a Changelog](https://qgis.org/en/site/forusers/visualchangelog320/index.html#feature-additional-options-for-opening-attribute-tables)

**Comentarios**

**Recursos**

Carpeta de recursos [/recursos/](/recursos/)

Enlaces a vídeos

![GUI for dynamic SVGs](/recursos/01/ejemplo.mp4)


### 50 Feature: Allow loading GPKG layers with GEOMETRY type

Categoría: **Data Management**

[Enlace a Changelog](https://qgis.org/en/site/forusers/visualchangelog320/index.html#feature-additional-options-for-opening-attribute-tables)

**Comentarios**

**Recursos**

Carpeta de recursos [/recursos/](/recursos/)

Enlaces a vídeos

![GUI for dynamic SVGs](/recursos/01/ejemplo.mp4)


### 51 Feature: Offline editing support for string list and number list field types

Categoría: **Data Management**

[Enlace a Changelog](https://qgis.org/en/site/forusers/visualchangelog320/index.html#feature-additional-options-for-opening-attribute-tables)

**Comentarios**

**Recursos**

Carpeta de recursos [/recursos/](/recursos/)

Enlaces a vídeos

![GUI for dynamic SVGs](/recursos/01/ejemplo.mp4)


### 52 Feature: Per-field usage of last values for newly created features

Categoría: **Forms and Widgets**

[Enlace a Changelog](https://qgis.org/en/site/forusers/visualchangelog320/index.html#feature-additional-options-for-opening-attribute-tables)

**Comentarios**

**Recursos**

Carpeta de recursos [/recursos/](/recursos/)

Enlaces a vídeos

![GUI for dynamic SVGs](/recursos/01/ejemplo.mp4)


### 53 Feature: JSON View widget

Categoría: **Forms and Widgets**

[Enlace a Changelog](https://qgis.org/en/site/forusers/visualchangelog320/index.html#feature-additional-options-for-opening-attribute-tables)

**Comentarios**

**Recursos**

Carpeta de recursos [/recursos/](/recursos/)

Enlaces a vídeos

![GUI for dynamic SVGs](/recursos/01/ejemplo.mp4)


### 54 Feature: Vector “label” legend entries

Categoría: **Layer Legend**

[Enlace a Changelog](https://qgis.org/en/site/forusers/visualchangelog320/index.html#feature-additional-options-for-opening-attribute-tables)

**Comentarios**

**Recursos**

Carpeta de recursos [/recursos/](/recursos/)

Enlaces a vídeos

![GUI for dynamic SVGs](/recursos/01/ejemplo.mp4)


### 55 Feature: Add option to save layer metadata to Package Layers algorithm

Categoría: **Processing**

[Enlace a Changelog](https://qgis.org/en/site/forusers/visualchangelog320/index.html#feature-additional-options-for-opening-attribute-tables)

**Comentarios**

**Recursos**

Carpeta de recursos [/recursos/](/recursos/)

Enlaces a vídeos

![GUI for dynamic SVGs](/recursos/01/ejemplo.mp4)


### 56 Feature: Raster Layer Properties Algorithm

Categoría: **Processing**

[Enlace a Changelog](https://qgis.org/en/site/forusers/visualchangelog320/index.html#feature-additional-options-for-opening-attribute-tables)

**Comentarios**

**Recursos**

Carpeta de recursos [/recursos/](/recursos/)

Enlaces a vídeos

![GUI for dynamic SVGs](/recursos/01/ejemplo.mp4)


### 57 Feature: Improved rasterize operation with 3D support

Categoría: **Processing**

[Enlace a Changelog](https://qgis.org/en/site/forusers/visualchangelog320/index.html#feature-additional-options-for-opening-attribute-tables)

**Comentarios**

**Recursos**

Carpeta de recursos [/recursos/](/recursos/)

Enlaces a vídeos

![GUI for dynamic SVGs](/recursos/01/ejemplo.mp4)


### 58 Feature: Save selected option for Package Layers Algorithm

Categoría: **Processing**

[Enlace a Changelog](https://qgis.org/en/site/forusers/visualchangelog320/index.html#feature-additional-options-for-opening-attribute-tables)

**Comentarios**

**Recursos**

Carpeta de recursos [/recursos/](/recursos/)

Enlaces a vídeos

![GUI for dynamic SVGs](/recursos/01/ejemplo.mp4)


### 59 Feature: Log levels for processing context

Categoría: **Processing**

[Enlace a Changelog](https://qgis.org/en/site/forusers/visualchangelog320/index.html#feature-additional-options-for-opening-attribute-tables)

**Comentarios**

**Recursos**

Carpeta de recursos [/recursos/](/recursos/)

Enlaces a vídeos

![GUI for dynamic SVGs](/recursos/01/ejemplo.mp4)


### 60 Feature: Geometry snapper rework offers major speed boost

Categoría: **Processing**

[Enlace a Changelog](https://qgis.org/en/site/forusers/visualchangelog320/index.html#feature-additional-options-for-opening-attribute-tables)

**Comentarios**

**Recursos**

Carpeta de recursos [/recursos/](/recursos/)

Enlaces a vídeos

![GUI for dynamic SVGs](/recursos/01/ejemplo.mp4)


### 61 Feature: Add a last_value function to the aggregate algorithm

Categoría: **Processing**

[Enlace a Changelog](https://qgis.org/en/site/forusers/visualchangelog320/index.html#feature-additional-options-for-opening-attribute-tables)

**Comentarios**

**Recursos**

Carpeta de recursos [/recursos/](/recursos/)

Enlaces a vídeos

![GUI for dynamic SVGs](/recursos/01/ejemplo.mp4)


### 62 Feature: Add global option to disable monitoring of directories

Categoría: **Application and Project Options**

[Enlace a Changelog](https://qgis.org/en/site/forusers/visualchangelog320/index.html#feature-additional-options-for-opening-attribute-tables)

**Comentarios**

**Recursos**

Carpeta de recursos [/recursos/](/recursos/)

Enlaces a vídeos

![GUI for dynamic SVGs](/recursos/01/ejemplo.mp4)


### 63 Feature: APIs for export and import of XML authentication configurations

Categoría: **Application and Project Options**

[Enlace a Changelog](https://qgis.org/en/site/forusers/visualchangelog320/index.html#feature-additional-options-for-opening-attribute-tables)

**Comentarios**

**Recursos**

Carpeta de recursos [/recursos/](/recursos/)

Enlaces a vídeos

![GUI for dynamic SVGs](/recursos/01/ejemplo.mp4)


### 64 Feature: Custom icon colors for folders in browser

Categoría: **Browser**

[Enlace a Changelog](https://qgis.org/en/site/forusers/visualchangelog320/index.html#feature-additional-options-for-opening-attribute-tables)

**Comentarios**

**Recursos**

Carpeta de recursos [/recursos/](/recursos/)

Enlaces a vídeos

![GUI for dynamic SVGs](/recursos/01/ejemplo.mp4)


### 65 Feature: OWS Group removed from the QGIS Browser

Categoría: **Browser**

[Enlace a Changelog](https://qgis.org/en/site/forusers/visualchangelog320/index.html#feature-additional-options-for-opening-attribute-tables)

**Comentarios**

**Recursos**

Carpeta de recursos [/recursos/](/recursos/)

Enlaces a vídeos

![GUI for dynamic SVGs](/recursos/01/ejemplo.mp4)


### 66 Feature: Add support for integer, real and integer64 list data types in OGR

Categoría: **Data Providers**

[Enlace a Changelog](https://qgis.org/en/site/forusers/visualchangelog320/index.html#feature-additional-options-for-opening-attribute-tables)

**Comentarios**

**Recursos**

Carpeta de recursos [/recursos/](/recursos/)

Enlaces a vídeos

![GUI for dynamic SVGs](/recursos/01/ejemplo.mp4)


### 67 Feature: Extend vector layer read capabilities to other layer types

Categoría: **Data Providers**

[Enlace a Changelog](https://qgis.org/en/site/forusers/visualchangelog320/index.html#feature-additional-options-for-opening-attribute-tables)

**Comentarios**

**Recursos**

Carpeta de recursos [/recursos/](/recursos/)

Enlaces a vídeos

![GUI for dynamic SVGs](/recursos/01/ejemplo.mp4)


### 68 Feature: External layer opacity

Categoría: **QGIS Server**

[Enlace a Changelog](https://qgis.org/en/site/forusers/visualchangelog320/index.html#feature-additional-options-for-opening-attribute-tables)

**Comentarios**

**Recursos**

Carpeta de recursos [/recursos/](/recursos/)

Enlaces a vídeos

![GUI for dynamic SVGs](/recursos/01/ejemplo.mp4)


### 69 Feature: Configurable Service URL

Categoría: **QGIS Server**

[Enlace a Changelog](https://qgis.org/en/site/forusers/visualchangelog320/index.html#feature-additional-options-for-opening-attribute-tables)

**Comentarios**

**Recursos**

Carpeta de recursos [/recursos/](/recursos/)

Enlaces a vídeos

![GUI for dynamic SVGs](/recursos/01/ejemplo.mp4)



# Licencia

Esta obra está bajo una licencia Creative Commons Attribution - Share Alike 3.0 - Unported license (CC BY-SA 3.0). El texto de la licencia está disponible en http://creativecommons.org/licenses/by-sa/3.0/ .

