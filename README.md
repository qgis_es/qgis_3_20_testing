# qgis_3_20_testing

## Objetivo

Actividad organizada la [Asociación QGIS España](https://www.qgis.es/) España probar y documentar las características de la version QGIS 3.20.0 Zürich (2021-06-18)

![QGIS 3.20](https://qgis.org/en/_images/712c5f48a25ce79a413e9cc34336e05100b7f0c1.png)

El listado de novedades se puede consultar en el siguiente enlace https://qgis.org/en/site/forusers/visualchangelog320/index.html

## ¿Qué queremos hacer?

Queremos probar las novedades y funcionalidades de la nueva versión de QGIS de forma colaborativa. 

Para ello, los que quieran colaborar elegirán una o varias de las nuevas funcionalidades descritas en el changelog (https://qgis.org/en/site/forusers/visualchangelog320/index.html)  y la testeará con datos propios, viendo cómo funcionan, si hay algún problema y buscando posibilidades y aplicaciones en su ámbito profesional.

La documentación y recursos creados quedarán reflejados en el documento [FEATURES](/FEATURES.md) de este repositorio.

Cuando lo tengamos proponemos hacer un evento online mostrando los resultados para que luego pueda ser colgado en el canal de Vimeo de la Asociación

## ¿Qué hago primero?

Pues instala o actualiza QGIS. Tienes los accesos desde aquí.
https://qgis.org/es/site/forusers/download.html

Parece que hay una nueva versión del instalador de OsGEO4W. Dale una vuelta en este mensaje en el foro https://lists.osgeo.org/pipermail/qgis-user/2021-February/048137.html

Es recomendable crear un perfil de usuario limpio para realizar las pruebas. Para ello ir al menú Configuración>Perfiles de Usuario>Nuevo perfil

## ¿Cómo nos organizamos?

El listado de novedades está en esta hoja de cálculo. 

https://docs.google.com/spreadsheets/d/1qghKhkMZRdNNcm0e-GVPd7x7cBRrGL8jbtw_Bk08k2k/edit?usp=sharing

Es cuestión de indicar que una categoría de funcionalidades y ponerte a probarlas.

Pon tu nombre en la columna de tester y márcala con color verde para saber qué funcionalidades se están probando.

## ¿Y si un grupo de funcionalidades está ya elegida por un tester?

No pasa nada, intenta buscar alguna otra que no lo esté. Y si no, puedes organizar con la persona que la ha seleccionado para probarla de forma conjunta. Algunas de ellas son muy extensas.

Pero vamos a intentar revisarlas todas Ok?

## ¿Dónde subir los datos que uso para probar las funcionalidades?

Los datos que se usen para las pruebas deben ser abiertos. 

El objetivo después de la sesión es que se suban en el repositorio GitLab de la Asociación (https://gitlab.com/qgis_es)  y que queden disponibles para que puedan ser usados por la comunidad.

Dentro del repositorio tienes la carpeta /SOURCES y dentro de ella puedes ir creando subcarpetas para cada feaure revisado.

**¡Ojo! Intenta que los datos ocupen poco**

Si usas datos, cuando finalices el testeo pégalos en su correspondiente subcarpeta

## ¿Hay plazos?

Sí, vamos a ponernos como tope el **lunes 6 de marzo**, tampoco es cuestión de que la cosa se eternice. 

Después hablamos con la Asociación para organizar la presentación.

## ¿Qué hago si detecto un error?

Mira primero en la página de issues por si alguien ya lo ha documentado. ej https://github.com/qgis/QGIS/issues/41858

- Si el error  no está documentado ya, o puedes aportar más información, Descríbelo, añade datos y copia la información del error que devuelve QGIS.
- Indica también en la hoja de cálculo si has detectado un error.
- Después de la revisión nos organizaremos para informar del error si no se encuentra documentado.

Información sobre información de errores
https://qgis.org/es/site/getinvolved/development/bugreporting.html#bugs-features-and-issues
https://github.com/qgis/QGIS/issues

## Recomendaciones

- Usa datos abiertos o públicos y documenta la fuente. Así si alguien quiere hacer pruebas puede hacerlas con los mismos datos.
- Aunque cueste, documenta lo que has hecho en un documento y haz capturas de pantalla. Lo aprovechamos para crear una entrada en el blog de la Asociación para ir preparando una presentación 
- ...
